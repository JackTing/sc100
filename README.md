SC100后台管理系统安装教程
----------------

## Requirements

* Ruby 2.4.0 +
* PostgreSQL 9.4 +
* Redis 2.8 +
* Memcached 1.4 +
* Elasticsearch 2.0 +

## 安装

### Ubuntu

```bash
$ sudo apt-get install memcached postgresql postgresql-contrib redis-server imagemagick ghostscript
```

安装 Elasticsearch

```bash
curl -sSL https://git.io/vVHhm | bash
```
安装 rbenv
```bash
$ git clone git://github.com/sstephenson/rbenv.git ~/.rbenv

$ git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build

$ git clone git://github.com/sstephenson/rbenv-gem-rehash.git ~/.rbenv/plugins/rbenv-$$$gem-rehash

$ git clone https://github.com/rkh/rbenv-update.git ~/.rbenv/plugins/rbenv-update

设置环境变量

 $ echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc     
 $ echo 'eval "$(rbenv init -)"' >> ~/.bashrc
 $ source ~/.bashrc

检查是否安装成功

 $ type rbenv

然后重开一个终端就可以执行 rbenv 了.

```
安装 Ruby
``` bash
安装

$ rbenv install -v 2.4.0

设置当前版本

rbenv global 2.4.0

安装 bundler(如果自动部署，需要先安装此gem)

gem install bundler

```

安装Node.js
``` bash
 git clone git://github.com/ry/node.git
 cd node
 ./configure
 make
 sudo make install
```
## 部署
 
创建账号
``` bash
假设你已经用 root 帐号通过 SSH 登陆服务器。

出于安全考虑，不要使用 root 帐号运行 web 应用。这里新建一个专门用于部署的用户,
例如 admin 或者其它你喜欢的名字(本项目采用admin账户)运行以下命令创建用户：

$ useradd -m -s /bin/bash admin

将用户加入 sudo 群组，以便使用 sudo 命令：

$ adduser admin sudo

为 admin 用户设置密码：

$ passwd admin

退出当前 SSH 链接，用 admin 帐号重新登陆。
```
安装Nginx

``` bash
 $ cd SC100
 $ mina provision:nginx
 
```

创建数据库
``` bash
  $ sudo -u postgres psql -c "create user admin with password 'SC100_2017'"
  $ sudo -u postgres psql -c "alter role admin with superuser;"
  $ sudo -u postgres psql -c "create database sc100_production owner admin;”
```
安装项目
``` bash
$ cd SC100
初始化 
$ mina setup
上传敏感文件
$ mina secrets:upload
部署程序
$ mina deploy

```
创建搜索数据索引
如果安装项目没有出现任何错误提示的话,则服务器的admin用户目录下会创建如下结构

``` bash
 - www
  - sc100
  	- current
  	- ...
ssh登录服务器
$ cd www/sc100/current
$ RAILS_ENV=production bundle exec rake environment elasticsearch:import:model CLASS='Model' FORCE=y
$ RAILS_ENV=production bundle exec rake environment elasticsearch:import:model CLASS='Material' FORCE=y

```

### 后台常用网址

* 管理页面
  http://su.simcity100.com/admin
* 列队查看
	http://su.simcity100.com/sidekiq
* 异常查看
  http://su.simcity100.com/exception-track
* 数据库查看
  http://su.simcity100.com/pghero






























