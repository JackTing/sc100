require 'mina/bundler'
require 'mina/rails'
require 'mina/git'
require 'mina/puma'
require 'mina/rbenv'  # for rbenv support. (https://rbenv.org)
# require 'mina/rvm'    # for rvm support. (https://rvm.io)

# Basic settings:
#   domain       - The hostname to SSH to.
#   deploy_to    - Path to deploy into.
#   repository   - Git repo to clone from. (needed by mina/git)
#   branch       - Branch name to deploy. (needed by mina/git)
set :app_root,'/Users/powermedia/Development/SC100'
set :application_name, 'sc100'
set :domain, 'su.simcity100.com'
set :deploy_to, '/home/admin/www/sc100'
set :repository, 'git@bitbucket.org:JackTing/sc100.git'
set :template_path, "#{fetch(:app_root)}/config/deploy/templates" # Local path to deploy templates

set :branch, 'master'
set :user, 'admin'    
set :forward_agent, true

set :shared_files, ['config/database.yml', 
                    'config/config.yml',
                    'config/elasticsearch.yml',
                    'config/memcached.yml',
                    'config/redis.yml',
                    'config/secrets.yml',
                    'config/sidekiq.yml']
set :shared_dirs,[
          'public/uploads',
          'public/system', 
          'log',
          'tmp'
        ]
require_relative 'deploy/sidekiq'
require_relative 'deploy/puma'
require_relative 'deploy/nginx'
require_relative 'deploy/secrets'
require_relative 'deploy/nodejs'
require_relative 'deploy/search'
# Optional settings:
#   set :user, 'foobar'          # Username in the server to SSH to.
#   set :port, '30000'           # SSH port number.
#   set :forward_agent, true     # SSH forward_agent.

# shared dirs and files will be symlinked into the app-folder by the 'deploy:link_shared_paths' step.
# set :shared_dirs, fetch(:shared_dirs, []).push('somedir')
# set :shared_files, fetch(:shared_files, []).push('config/database.yml', 'config/secrets.yml')

# This task is the environment that is loaded for all remote run commands, such as
# `mina deploy` or `mina rake`.
task :environment do
  # If you're using rbenv, use this to load the rbenv environment.
  # Be sure to commit your .ruby-version or .rbenv-version to your repository.
  invoke :'rbenv:load'

  # For those using RVM, use this to load an RVM version@gemset.
  # invoke :'rvm:use', 'ruby-1.9.3-p125@default'
end

# Put any custom commands you need to run at setup
# All paths in `shared_dirs` and `shared_paths` will be created on their own.

desc "Deploys the current version to the server."
task :deploy do
  # uncomment this line to make sure you pushed your local branch to the remote origin
  # invoke :'git:ensure_pushed'
  deploy do
    # Put things that will set up an empty directory into a fully set-up
    # instance of your project.
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    invoke :'rails:db_migrate'
    invoke :'rails:assets_precompile'
    invoke :'deploy:cleanup'

    on :launch do
      invoke :'puma:restart'
      # 启动sidekiq
      invoke :'sidekiq:quiet'
      invoke :'sidekiq:restart'
      #
      in_path(fetch(:current_path)) do
        command %{mkdir -p tmp/pids/}
        command %{mkdir -p tmp/sockets/}
      end
    end
  end

  # you can use `run :local` to run tasks on local machine before of after the deploy scripts
  # run(:local){ say 'done' }
end

# For help in making your deploy script, see the Mina documentation:
#
#  - https://github.com/mina-deploy/mina/tree/master/docs
