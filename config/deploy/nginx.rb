###
### NGINX
################################################################################

namespace :nginx do
  %w(stop start restart reload status).each do |action|
    desc "#{action.capitalize} NGINX"
    task action.to_sym => :environment do
      command  %(echo "-----> #{action.capitalize} NGINX")
      command "sudo service nginx #{action}"
    end
  end
end

namespace :provision do
  desc "Install NGINX"
  task :nginx do
    command "sudo add-apt-repository -y ppa:nginx/stable"
    command "sudo apt-get update -y"
    command "sudo apt-get install -y nginx"
    command "sudo apt-get clean -y"

    nginx_puma = erb("#{fetch(:template_path)}/nginx_puma.erb")

    command %[echo '#{nginx_puma}' > /home/admin/nginx_conf]
    command %[sudo mv /home/admin/nginx_conf /etc/nginx/sites-enabled/#{fetch(:application_name)}]
    command %[sudo rm -f /etc/nginx/sites-enabled/default]
  end
end
