###
### POSTGRESQL
################################################################################

set :pg_database,          "#{fetch(:app_name)}_production"
set :pg_user,              fetch(:app_name)
set :pg_host,              'localhost'

namespace :provision do
  desc "Install postgresql"
  task :postgresql do
    command "wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -"
    command "sudo sh -c 'echo \"deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main\" >> /etc/apt/sources.list.d/pgdg.list'"
    command "sudo apt-get update -y"
    command "sudo apt-get -y install libpq-dev postgresql-9.4"
    command "sudo apt-get clean -y"

    set :pg_password, ask("Postgresql password: ") { |q| q.default = "password" }
    
    command %Q{sudo -u postgres psql -c "create user #{app_name} with password '#{pg_password}';"}
    command %Q{sudo -u postgres psql -c "alter role #{app_name} with superuser;"}
    command %Q{sudo -u postgres psql -c "create database #{app_name}_production owner #{app_name};"}

    database_yml = erb("#{template_path}/postgresql.yml.erb")
    command "echo '#{database_yml}' > #{deploy_to}/#{shared_path}/config/database.yml"
    command "cat #{deploy_to}/#{shared_path}/config/database.yml"
  end
end