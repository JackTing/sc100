###
### PUMA
################################################################################

namespace :puma do
  desc "Update puma config."
  task :config do
    puma_config = erb("#{fetch(:template_path)}/puma.rb.erb")
    command %[echo '#{puma_config}' > #{fetch(:deploy_to)}/shared/config/puma.rb]
    invoke :'puma:restart'
  end
end