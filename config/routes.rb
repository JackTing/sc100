Rails.application.routes.draw do
 
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      get 'hello', to: 'root#hello'
      resources :nodes,only: [:index,:show] do
        collection do
          get :materials
          get :models
        end
      end
      resources :materials,only:[:index,:show]
      resources :models,only:[:index,:show]
    end
  end

  root to: 'materials#index'
  get 'materials/material_category:id',to: 'materials#node_category', as: 'node_category_materials'
  get 'materials/node:id', to: 'materials#node', as: 'node_materials'
  get 'models/model_category:id',to: 'models#node_category', as: 'node_category_models'
  get 'models/node:id', to: 'models#node', as: 'node_models'
  resources :materials,only:[:index,:new,:create] do
    collection do
      get :all
      get :search
      get :find_by_ids
    end
  end
  resources :models,only:[:index,:new,:create] do
    collection do
      get :all
      get :search
      get :find_by_ids
    end
  end
  resources :nodes,only:[:index]
  resources :categories,only:[:index]
  resources :home,only:[:index]

  devise_for :admin_users, ActiveAdmin::Devise.config
  devise_for :users
  begin
    ActiveAdmin.routes(self) 
  rescue Exception => e
    
  end

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
  mount PgHero::Engine, at: "pghero"
  mount ExceptionTrack::Engine => "/exception-track"

end
