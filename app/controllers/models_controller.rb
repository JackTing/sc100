class ModelsController < ApplicationController
	layout "upload", :only => [:new]
	before_action :get_categories,:only => [:index,:node,:node_category,:search,:new,:find_by_ids]
	def index
		if session[:last_model_node_id]
			@node = Node.find(session[:last_model_node_id])
			@models = @node.models.recent.page(params[:page])
		else
			@models = Model.recent.page(params[:page])
		end
		fresh_when([@model_categories, @models])
	end

  def node_category
    @node = ModelCategory.find(params[:id])
    @models = Model.include_column_ids('node_id',@node.nodes.ids)
    @models = @models.recent.page(params[:page])
    if stale?(etag: [@nodel, @model_categories,@models], template: 'models/index')
      render action: 'index'
    end
  end

	def node
		@node = Node.find(params[:id])
    @models = @node.models
    @models = @models.recent.page(params[:page])
    session[:last_model_node_id] = @node.id
    if stale?(etag: [@node, @model_categories,@models], template: 'models/index')
      render action: 'index'
    end
	end

	def find_by_ids
		ids = params[:ids]? params[:ids].split(',') : []
		@models = Model.find_by_ids(ids).recent.page(params[:page])
    respond_to do |format|
      format.js
    end
	end

	def search
    search_params = {
      query: {
        simple_query_string: {
          query: params[:q],
          default_operator: 'AND',
          minimum_should_match: '20%',
          fields: %w(display_name remark)
        }
      }
    }
    @models = Elasticsearch::Model.search(search_params, [Model]).page(params[:page])
    #if stale?(etag: ['search_models',@models], template: 'models/index')
    render action: 'index'
    #end
	end

	def all
		session[:last_model_node_id]=nil
		redirect_to models_path
	end

	def new
    @model = Model.new
  end

	def create
    @model = Model.new(model_params)
    respond_to do |format|
      if @model.save
        format.js
      else
        format.js
      end
    end
  end
	private 
	def get_categories
		@model_categories = ModelCategory.all.sorted
	end
	def model_params
    params.require(:model).permit(:name,:node_id,:skp,:remark,:thumbnail,:display_name)
  end
end
