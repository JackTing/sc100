class MaterialsController < ApplicationController
	layout "upload", :only => [:new,:create]
	before_action :get_categories,:only => [:index,:node_category,:node,:search,:new,:find_by_ids]
	def index
		if session[:last_mat_node_id]
			@node = Node.find(session[:last_mat_node_id])
			unless @node.blank?
				@materials = @node.materials.recent.page(params[:page])
				session[:last_mat_node_id] = @node.id
			else
				@materials = Material.recent.page(params[:page])
			end
		else
			@materials = Material.recent.page(params[:page])
			session[:last_mat_page] = params[:page]
		end
		fresh_when([@material_categories, @materials])
	end

	def node_category
		@node = MaterialCategory.find(params[:id])
		@materials = Material.include_column_ids('node_id',@node.nodes.ids)
		@materials = @materials.recent.page(params[:page])
		if stale?(etag: [@nodel, @material_categories,@materials], template: 'materials/index')
      render action: 'index'
    end
	end

	def node
		@node = Node.find(params[:id])
    @materials = @node.materials
    @materials = @materials.recent.page(params[:page])
    session[:last_mat_node_id] = @node.id
		session[:last_mat_page]    = params[:page]
    if stale?(etag: [@node, @material_categories,@materials], template: 'materials/index')
      render action: 'index'
    end
	end

	def all
		session[:last_mat_node_id] = nil
		session[:last_mat_page]    = nil
		redirect_to materials_path
	end

	def find_by_ids
		ids = params[:ids]? params[:ids].split(',') : []
		@materials = Material.find_by_ids(ids).recent.page(params[:page])
  	respond_to do |format|
      format.js
    end
	end

	def search
    search_params = {
      query: {
        simple_query_string: {
          query: params[:q],
          default_operator: 'AND',
          minimum_should_match: '20%',
          fields: %w(display_name remark)
        }
      }
    }
    @materials = Elasticsearch::Model.search(search_params, [Material]).page(params[:page])
    #if stale?(etag: ['search_materials',@materials], template: 'materials/index')
      render action: 'index'
    #end
	end

	def new
    @material = Material.new
  end

	def create
    @material = Material.new(material_params)
    respond_to do |format|
      if @material.save
        format.js
      else
        format.js
      end
    end
  end
	private 
	# def get_page
	# 	@page     = session[:last_mat_page].to_i
	# 	new_page  = params[:page].to_i
	# 	new_page =1 if new_page ==0
	# 	return if new_page == @page
	# 	if (new_page-@page).abs==1
	# 		@page = new_page
	# 	end
	# end
	def get_categories
		@material_categories = MaterialCategory.all.sorted
	end
	def material_params
    params.require(:material).permit(:name,:node_id,:color,:alpha,:texture,:remark,:thumbnail,:display_name)
  end
end
