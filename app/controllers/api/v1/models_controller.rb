module Api
  module V1
    class ModelsController < Api::V1::ApplicationController

      # 获取 Models 列表
      #
      # GET /api/v1/models
      # @return [Array<ModelSerializer>]
      def index
        @models = Model.all
        @meta = { total: Model.count }
      end

      ##
      # 获取单个 Model 详情
      #
      # GET /api/v1/model/:id
      # @return [ModelSerializer]
      def show
        @model = Model.find(params[:id])
      end
    end
  end
end
