module Api
  module V1
    class MaterialsController < Api::V1::ApplicationController

      # 获取 Materials 列表
      #
      # GET /api/v1/materials
      # @return [Array<MaterialSerializer>]
      def index
        @materials = Material.all
        @meta = { total: Material.count }
      end

      ##
      # 获取单个 Material 详情
      #
      # GET /api/v1/material/:id
      # @return [MaterialSerializer]
      def show
        @material = Material.find(params[:id])
      end
    end
  end
end
