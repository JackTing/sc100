module Api
  module V1
    class RootController < Api::V1::ApplicationController
      #before_action :doorkeeper_authorize!, only: [:hello]

      def not_found
        raise ActiveRecord::RecordNotFound
      end

      # 简单的 API 测试接口，需要验证，便于快速测试 OAuth 以及其他 API 的基本格式是否正确
      #
      # GET /api/v1/hello
      #
      # @param limit - API token
      # @return [UserDetailSerializer]
      def hello
        optional! :limit, values: 0..100

        @meta = Time.now
        # @user = current_user
        render "api/v1/hello/hello"
      end

    end
  end
end
