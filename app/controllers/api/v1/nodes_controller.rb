module Api
  module V1
    class NodesController < Api::V1::ApplicationController

      # 获取 Nodes 列表
      #
      # GET /api/v1/nodes
      # @return [Array<NodeSerializer>]
      def index
        @nodes = Node.all
        @meta = { total: Node.count }
      end

      # 获取材质 Nodes 列表
      #
      # GET /api/v1/nodes/materials
      # @return [Array<NodeSerializer>]
      def materials
        @nodes = Node.material_nodes
        @meta  = { total: @nodes.count }
        render "api/v1/nodes/index"
      end

      # 获取材质 Nodes 列表
      #
      # GET /api/v1/nodes/models
      # @return [Array<NodeSerializer>]
      def models
        @nodes = Node.model_nodes
        @meta  = { total: @nodes.count }
        render "api/v1/nodes/index"
      end

      ##
      # 获取单个 Node 详情
      #
      # GET /api/v1/nodes/:id
      # @return [NodeSerializer]
      def show
        @node = Node.find(params[:id])
      end
    end
  end
end
