module ApplicationHelper
	EMPTY_STRING = ''.freeze
  def cache(name = {}, options = {}, &block)
    options ||= {}
    super([I18n.locale, name], options, &block)
  end
  
	def render_list_items(list = [])
    yield(list) if block_given?
    items = []
    list.each do |link|
      item_class = EMPTY_STRING
      urls = link.match(/href=(["'])(.*?)(\1)/) || []
      url = urls.length > 2 ? urls[2] : nil
      if url && current_page?(url) || controller_name && ["/#{controller_name}"].include?(url)
        item_class = 'active'
      end
      items << content_tag('li', raw(link), class: item_class)
    end
    raw items.join(EMPTY_STRING)
  end
end
