class SearchIndexer < ApplicationJob
  queue_as :search_indexer

  def perform(operation, type, id)
    obj = nil
    type.downcase!

    case type
    when 'model'
      obj = Model.find_by_id(id)
    when 'material'
      obj = Material.find_by_id(id)
    end
    
    return unless obj

    if operation == 'update'
      obj.__elasticsearch__.update_document
    elsif operation == 'index'
      obj.__elasticsearch__.index_document
    elsif operation == 'delete'
      obj.__elasticsearch__.delete_document
    end
  end
end
