module BaseModel
	extend ActiveSupport::Concern

	included do
		scope :recent, -> { order(id: :desc) }
		scope :exclude_ids, ->(ids) { where.not(id: ids.map(&:to_i)) }
		scope :by_week, -> { where("created_at > ?", 7.days.ago.utc) }
		scope :find_by_ids, ->(ids) { include_column_ids('id', ids) }
  	scope :include_column_ids, ->(column, ids) { ids.empty? ? all : where(column => ids) }
		
		delegate :url_helpers, to: 'Rails.application.routes'
	end
	
end
