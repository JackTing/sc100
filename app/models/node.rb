class Node < ApplicationRecord
	include BaseModel
  belongs_to :material_category, :touch => true 
  belongs_to :model_category, :touch => true 
  has_many   :materials
  has_many   :models

  scope :material_nodes, -> { where(model_category_id: nil) }
  scope :model_nodes,->{where(material_category_id: nil)}
  # 缓存
  second_level_cache expires_in: 2.days
  scope :sorted, -> { order(sort: :desc) }
end
