class Model < ApplicationRecord
	include Searchable
  include BaseModel
  belongs_to :node

  
  mount_uploader :skp,SkpFileUploader,:on=>:filename
  mount_uploader :thumbnail,PhotoUploader,:on=>:filename

  validates :display_name,presence: true
  validates :node_id,presence: true
  validates :name,presence: true
  validates :remark,presence: true
  # validates :thumbnail,presence: true
  # validates :skp,presence: true

  after_save :update_cache_version
  after_destroy :update_cache_version
  def update_cache_version
    # 记录节点变更时间，用于清除缓存
    CacheVersion.models_updated_at = Time.now.to_i
  end

  def indexed_changed?
    display_name_changed? || remark_changed?
  end
end
