class MaterialCategory < ApplicationRecord
		include BaseModel
    has_many :nodes
    # 缓存
    second_level_cache expires_in: 2.days
    
    scope :sorted, -> { order(sort: :desc) }
    def self.nodes
        Node.where("material_category_id IS NOT NULL").order(model_category_id: :desc)
    end
end
