class Material < ApplicationRecord
	include Searchable
  include BaseModel
  belongs_to :node
  mount_uploader :texture,PhotoUploader,:on=>:filename
  mount_uploader :thumbnail,PhotoUploader,:on=>:filename
  
  validates :node_id,presence: true
  validates :name,presence: true
  validates :display_name,presence: true
  #validates :thumbnail,presence: true
  validates :remark,presence: true


  after_save :update_cache_version
  after_destroy :update_cache_version
  def update_cache_version
    # 记录节点变更时间，用于清除缓存
    CacheVersion.materials_updated_at = Time.now.to_i
  end

  def indexed_changed?
    display_name_changed? || remark_changed?
  end

end
