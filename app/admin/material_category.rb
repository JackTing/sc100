ActiveAdmin.register MaterialCategory do
  menu priority: 2, label: "材质分类"
	# See permitted parameters documentation:
	# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
	#
	permit_params :name,:sort
	#
	# or
	#
	# permit_params do
	#   permitted = [:permitted, :attributes]
	#   permitted << :other if params[:action] == 'create' && current_user.admin?
	#   permitted
	# end
	show do
	  render 'show', { material_category: material_category }
	end

end
