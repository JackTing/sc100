ActiveAdmin.register Node do
  menu priority: 4, label: "节点"
	# See permitted parameters documentation:
	# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
	#
	permit_params :name,:sort,:material_category_id,:model_category_id
	#
	# or
	#
	# permit_params do
	#   permitted = [:permitted, :attributes]
	#   permitted << :other if params[:action] == 'create' && current_user.admin?
	#   permitted
	# end
	filter :material_category, as: :select,label: "材质分类"
	filter :model_category, as: :select,label: "模型分类"

	show do
	  render 'show', { node: node }
	end

end
