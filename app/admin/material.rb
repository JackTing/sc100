ActiveAdmin.register Material do
  menu priority: 6, label: "材质"
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :name,:node_id,:color,:alpha,:texture,:remark,:thumbnail,:display_name

  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  index do
    selectable_column
    column :display_name
    column :name
    column :color do |material|
      color_field_tag 'color', material.color
    end
    column :alpha
    column :thumbnail do |material|
      if material.thumbnail.file
        image_tag(material.thumbnail.url(:md))
      end
    end
    column :texture do |material|
      if material.texture.file
        image_tag(material.texture_url(:md)) 
      end
    end
    column :remark
    actions
  end
  
  # show do
  #   attributes_table do
  #     row :display_name
  #     row :name
  #     row :color do |ad|
  #       color_field_tag 'color', ad.color
  #     end
  #     row :alpha
  #     row :thumbnail do |ad|
  #       image_tag ad.thumbnail.url(:md) if ad.thumbnail.file
  #     end
  #     row :texture do |ad|
  #       image_tag ad.texture.url(:md) if ad.texture.file
  #     end
  #     row :remark
  #   end

  # end
  show do
    render 'show', { material: material }
  end

  form do |f|
    f.inputs "详情" do
      f.input :node_id,:label=>"所属节点", as: :select, collection: MaterialCategory.nodes.map{|n| ["#{n.material_category.name}-#{n.name}",n.id]}
      f.input :display_name,:label=> "显示名称"
      f.input :name,:label=>"材料名称"
      f.input :color,:label=>"颜色值", input_html: {style:"padding:0px;"}
      f.input :alpha ,:label=>"透明度",value: 100
      f.input :thumbnail,:label=>"预览图",as: :file
      f.input :texture,:label=>"贴图",as: :file
      f.input :remark,:label=>"备注"
    end
    f.actions
  end
  filter :node_id, as: :select,
  collection:  Node.material_nodes.map{|n| ["#{n.material_category.name}-#{n.name}",n.id]},
 label:      '分类'
end
