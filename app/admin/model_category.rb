ActiveAdmin.register ModelCategory do
  menu priority: 3, label: "模型分类"
	# See permitted parameters documentation:
	# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
	#
	permit_params :name,:sort
	#
	# or
	#
	# permit_params do
	#   permitted = [:permitted, :attributes]
	#   permitted << :other if params[:action] == 'create' && current_user.admin?
	#   permitted
	# end
	show do
	  render 'show', { model_category: model_category }
	end
  form do |f|
    f.inputs "详情" do
      f.input :name,:label=>"名称"
      f.input :sort,:label=>"排序"
    end
    f.actions
  end

end
