ActiveAdmin.register_page "Dashboard" do

  menu priority: 0, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    div class: "blank_slate_container", id: "dashboard_default_message" do
      span class: "blank_slate" do
        span I18n.t("active_admin.dashboard_welcome.welcome")
      end
    end

    # Here is an example of a simple dashboard with columns and panels.
    
    # columns do
    #   column do
    #     panel "模型分类" do
    #       table_for ModelCategory.order('id desc').limit(5) do
    #         column :name do |mc| 
    #             link_to(mc.name, admin_model_category_path(mc))
    #         end 
    #         column :sort
    #       end
    #     end
    #   end
    #   column do
    #     panel "材质分类" do
    #       table_for MaterialCategory.order('id desc').limit(5) do
    #         column :name do |mc| 
    #             link_to(mc.name, admin_material_category_path(mc))
    #         end 
    #         column :sort
    #       end
    #     end
    #   end
    # end
  end # content
end
