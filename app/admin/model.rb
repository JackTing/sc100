ActiveAdmin.register Model do
  menu priority: 5, label: "模型"
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :name,:node_id,:skp,:thumbnail,:remark,:display_name
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
  index do
    selectable_column
    column :display_name
    column :name
    column :thumbnail do |model|
      if model.thumbnail.file
        image_tag(model.thumbnail.url(:md))
      end
    end
    column :remark
    actions
  end


  # show do
  #   attributes_table do
  #     row :display_name
  #     row :name
  #     row :thumbnail do |ad|
  #       image_tag ad.thumbnail.url(:md) if ad.thumbnail.file
  #     end
  #     row :skp
  #     row :remark
  #   end
  # end
  show do
    render 'show', { model: model }
  end

  form do |f|
    f.inputs "详情" do
      f.input :node_id,:label=>"所属节点", as: :select, collection: ModelCategory.nodes.map{|n| ["#{n.model_category.name}-#{n.name}",n.id]}
      f.input :display_name,:label=>"显示名称"
      f.input :name,:label=>"定义名称"
      f.input :thumbnail,:label=>"预览图",as: :file
      f.input :skp,:label=>"skp模型",as: :file
      f.input :remark,:label=>"备注"
    end
    f.actions
  end
  filter :node_id, as: :select,
   collection:  Node.model_nodes.map{|n| ["#{n.model_category.name}-#{n.name}",n.id]},
   label:      '分类'

end
