    //window  document   jquery都属于全局对象
    var Scroll = {};
    (function(win, doc, $) { //把经常用到的函数或者方法作为形参传入，方便js快速查找
        function ScrollBar(options) {
            console.log(options);
            this._init(options);
        }
        $.extend(ScrollBar.prototype, {
            _init: function(options) {
                var self = this;
                self.options = {
                    contSelector: "", //滑动内容区块
                    barSelector: "", //滑动条选择器
                    sliderSelector: "", //滑动块选择器
                    tabItemSelector: "", //标签选择器
                    tabActiveClass: "", //选中的标签样式
                    anchorSelector: "", //锚点选择器
                    wheelStep: 30 //滚动步伐
                };
                $.extend(true, self.options, options || {}); //把参数初始化
                self._initDomEvent(); //初始化DOM
                self._initSliderDragEvent(); //绑定滑块点击拖动事件
                self._bandMouseWheel(); //绑定滚轮事件
                self._bandContScroll(); //监听内容滚动
                self._initTabEvent(); //点击tab切换
                return self;
            },
            /*
                初始化DOM
            */
            _initDomEvent: function() {
                var opts = this.options;
                this.$cont = $(opts.contSelector);
                this.$slider = $(opts.sliderSelector);
                this.$bar = opts.barSelector ? $(opts.barSelector) : this.$slider.parent();
                this.$doc = $(doc);
                this.$tabItem = $(opts.tabItemSelector);
                this.$anchor = $(opts.anchorSelector);
            },
            _initSliderDragEvent: function() {
                var self = this; //区分作用域
                var slider = self.$slider; //滑块
                var cont = self.$cont; //内容区域
                var doc = self.$doc, //document对象
                    dragStartPagePosition, //当前位置
                    dragStartScrollPosition, //滚动位置
                    dragContBarRate; //滚动条
                function mousemoveHandler(e) {
                    if (dragStartPagePosition == null) {
                        return;
                    }
                    self.scrollContTo(dragStartScrollPosition + (e.pageY - dragStartPagePosition) * dragContBarRate); //拖动之前的位置+拖动距离=拖动之后的位置
                }
                //移动之前的高度和移动时的高度对比
                slider.on("mousedown", function(event) { //按下事件
                    event.preventDefault();
                    dragStartPagePosition = event.pageY; //获取y坐标，记录的是起始位置
                    dragStartScrollPosition = cont[0].scrollTop; //获取当前滚动高度
                    dragContBarRate = self.getMaxScrollPosition() / self.getMaxSliderPosition();

                    doc.on("mousemove.scroll", function(event) { //移动事件
                        event.preventDefault();
                        mousemoveHandler(event);
                    }).on("mouseup.scroll", function(event) { //鼠标离开事件
                        event.preventDefault();
                        doc.off(".scroll"); //关闭滑动
                    });
                });
            },
            /*
            绑定监听事件方法
            */
            _bandContScroll: function() {
                var self = this;
                self.$cont.on("scroll", function(e) { //监听滚动事件，根据滚动事件返回要改变的Top值
                    e.preventDefault();
                    self.$slider.css('top', self.getSliderPosition() + 'px');
                });
            },
            /*
            绑定滚轮事件
            */
            _bandMouseWheel: function() {
                var self = this;
                /*
                mousewheel：chrome 3
                DOMMouseScroll:Firefox 
                滚动的步伐不一致
                */
                self.$cont.on("mousewheel DOMMouseScroll", function(e) { //滚轮事件
                    e.preventDefault();
                    var oEv = e.originalEvent; //指定原始的事件对象
                    var wheelRange = oEv.wheelDelta ? -oEv.wheelDelta / 120 : (oEv.detail || 0) / 3; //步伐等于1
                    self.scrollContTo(self.$cont[0].scrollTop + wheelRange * self.options.wheelStep);
                });
                return self;
            },
            /*
            初始化tab点击切换
            */
            _initTabEvent: function() {
                var self = this;
                self.$tabItem.on("click", function(e) {
                    e.preventDefault();
                    var index = $(this).index(); //拿到当前的下标
                    self.changeTabSelect(index); //切换tab
                    self.scrollContTo(self.$cont[0].scrollTop + self.getAnchorPosition(index)); //跳转到相应位置
                });
            },
            changeTabSelect: function(index) { //切换tab菜单
                var self = this;
                var active = self.options.tabActiveClass; //拿到设置要增加的样式

                self.$tabItem.eq(index).addClass(active).siblings().removeClass(active); //切换tab状态
                this.$tabItem.last().next(".st-status").css({
                    "left": index * 115 + 5 + "px"
                });
                return self;
            },
            getAllAnchorPosition: function() {
                var self = this;
                var allAnchor = [];
                for (var i = 0; i < self.$anchor.length; i++) {
                    allAnchor.push(self.$cont[0].scrollTop + self.getAnchorPosition(i));
                }
                return allAnchor;
            },
            getAnchorPosition: function(index) {
                var self = this;
                return self.$anchor.eq(index).position().top;
            },
            getSliderPosition: function() {
                var self = this;
                return self.$cont[0].scrollTop / (self.getMaxScrollPosition() / self.getMaxSliderPosition());
            },
            /*
             获取最大滚动位置
            */
            getMaxScrollPosition: function() {
                var self = this;
                return Math.max(self.$cont.height(), self.$cont[0].scrollHeight) - self.$cont.height();
            },
            getMaxSliderPosition: function() {
                var self = this;
                return self.$bar.height() - self.$slider.height();
            },
            /*
            切换整个状态
            */
            scrollContTo: function(positionVal) { //传入位置，跳转到相应位置
                var self = this;
                var anchorArr = self.getAllAnchorPosition(); //获取所有锚点的信息
                //console.log(anchorArr);
                function getIndex(positionVal) { //获取跳转到那个位置
                    for (var i = anchorArr.length - 1; i >= 0; i--) {
                        if (positionVal >= anchorArr[i] + (i == 0 ? anchorArr[i] - anchorArr[i + 1] : anchorArr[i - 1] - anchorArr[i]) / 2) { //获取下一个一半的高度，然后判断
                            return i; //返回第几个
                        }
                    }
                }
                if (self.$tabItem.length == anchorArr.length) { //判断个数是否相等
                    self.changeTabSelect(getIndex(positionVal)); //切换标签
                    self.$cont.scrollTop(positionVal); //跳转到滚动指定位置
                }
            }
        });
        Scroll.ScrollBar = ScrollBar;
    })(window, document, jQuery);
