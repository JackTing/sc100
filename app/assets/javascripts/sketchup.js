/*
SC JavaScript Library for SketchUp WebDialogs
Version:      1.0.0
Date:         08.07.2016
*/

/**
* @namespace
*/
var SC = function (SC) {
  SC.IS_CHROME=function(){
    var isChrome = window.navigator.userAgent.indexOf("Chrome") !== -1;
    return isChrome;
  };
  SC.UI = function (self) {
    self.messageBox=function(data){
      self.showMsg(data.message,data.type);
    };
    self.showMsg = function (msg, type) {
      var tclass = type == 1 ? 'mgAlert-error' : 'mgAlert-success',
      dom = $('#mgAlert')[0];
      dom.setAttribute('class', 'mgAlert ' + tclass + ' mshow');
      dom.innerHTML = msg;
      setTimeout(function () {
        dom.setAttribute('class', 'mgAlert');
        dom.innerHTML = '';
      }, 2000);
    };
    /* Disables the context menu with the exception for textboxes in order to
    * mimic native windows.
    */
    self.disable_context_menu = function () {
      $(document).on('contextmenu', function (e) {
        return $(e.target).is('input[type=text], textarea');
      });
    };
    self.initializerData = function(data){
      // 初始化材质填充模式
      self.set_view_mod(data['view_mat_mode']);
      self.set_view_mod(data['view_mod_mode']);
    };
    self.set_view_mod = function(view_mode){
      switch (view_mode){
        case 'small_mat':
          $("#big_mat").removeClass("big").addClass('big-hover');
          $("#small_mat").removeClass("small-hover").addClass('small');
          $(".ul-img-all1").removeClass("ul-switch").addClass('ul-img');
          break;
        case 'big_mat':
          $("#small_mat").removeClass("small").addClass('small-hover');
          $("#big_mat").removeClass("big-hover").addClass('big');
          $(".ul-img-all1").removeClass("ul-img").addClass('ul-switch');
          break;
        case 'small_mod':
          $("#big_mod").removeClass("big").addClass('big-hover');
          $("#smll_mod").removeClass("small-hover").addClass('small');
          $(".ul-img-all2").removeClass("ul-switch").addClass('ul-img');
          break;
        case 'big_mod':
          $("#small_mod").removeClass("small").addClass('small-hover');
          $("#big_mod").removeClass("big-hover").addClass('big');
          $(".ul-img-all2").removeClass("ul-img").addClass('ul-switch');
          break;
      }
    };
    self.redirect_to = function(data){
       // window.location.href = data['url']
       Turbolinks.visit(data['url'])
    };
    self.find_by_ids = function(data){
      $.ajax({
        type: "get",
        url: data['url'],
        data: { 'ids':data['ids'] },
        error: function(){
          alert("未知错误!");
        }
      });
    };
    return self;
  }(SC.UI || {}); // UI

  SC.Bridge = function (self) {
    // Creates a hidden textarea element used to pass data from JavaScript to
    // Ruby. Ruby calls UI::WebDialog.get_element_value to fetch the content and
    // parse it as JSON.
    // This avoids many issues in regard to transferring data:
    // * Data can be any size.
    // * Avoid string encoding issues.
    // * Avoid evaluation bug in SketchUp when the string has a single quote.
    self.init = function () {
      var $bridge = $("<textarea id='SU_BRIDGE'></textarea>");
      $bridge.hide();
      $("body").append($bridge);
    };
    self.set_data = function (data) {
      var $bridge = $("#SU_BRIDGE");
      $bridge.text("");
      if (data !== undefined) {
        var json = JSON.stringify(data);
        $bridge.text(json);
      }
    };
    return self;
  }(SC.Bridge || {}); // Bridge

  SC.Sketchup = function (self) {
    self.callback = function (event_name, data) {
      // Defer with a timer in order to allow the UI to update.
      setTimeout(function () {
        if(SC.IS_CHROME()&&(typeof sketchup != 'undefined')){
          sketchup.callback(event_name, data, {
            onCompleted: function() {
              //console.log('Ruby side done.');
            }
          });
        }else{
          SC.Bridge.set_data(data);
          window.location = "skp:callback@" + event_name;
        }
      }, 0);
    };
    return self;
  }(SC.Sketchup || {});
  return SC;
}(SC || {});

/*******************************************************************************
*
* initializer
*
******************************************************************************/
window.clickEvent ='click';
window.changeEvent ='change';
window.keyDown = 'keydown';

/**
* [onerror]        
*/
window.onerror = function (message, file, line, column, error) {
  try {
    var backtrace = [];
    if (error && error.stack) {
      backtrace = String(error.stack).split("\n");
    } else {
      backtrace = [file + ':' + line + ':' + column];
    }
    var data = {
      'message': message,
      'backtrace': backtrace
    };
    SC.Sketchup.callback('Window.js_error', data);
  } catch (error) {
    debugger;
    throw error;
  }
};