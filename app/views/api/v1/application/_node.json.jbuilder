# @class NodeSerializer
# 节点
#
# == attributes
# - *id* [Integer] 编号
# - *name* [String] 节点名称
# - *sort* {Integer} 排序优先级
# - *updated_at* [DateTime] 更新时间
if node
  json.cache! ['v1', node] do
    json.(node, :id, :name, :sort, :updated_at)
  end
end
