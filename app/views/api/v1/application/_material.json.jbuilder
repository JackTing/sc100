# @class MaterialSerializer
# 材质
#
# == attributes
# - *id* [Integer] 编号
# - *name* [String] 节点名称
# - *color* [String] 颜色值
# - *texture_url* [String] 贴图文件地址
# - *thumbnail_url* [String] 预览图文件地址
# - *display_name* [String] 显示名称
if material
  json.cache! ['v1', material] do
    json.(material, :id, :name,:color,:texture_url,:thumbnail_url,:alpha,:display_name)
  end
end
