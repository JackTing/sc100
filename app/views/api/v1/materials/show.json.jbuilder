json.material do
  json.partial! partial: 'material', locals: { material: @material }
end
json.meta @meta