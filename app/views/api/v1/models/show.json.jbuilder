json.model do
  json.partial! partial: 'model', locals: { model: @model }
end
json.meta @meta