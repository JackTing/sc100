class CreateModels < ActiveRecord::Migration[5.1]
  def change
    create_table :models do |t|
      t.string :name
      t.integer :node_id
      t.string :skp
      t.string :thumbnail
      t.text   :remark
      t.timestamps
    end
  end
end
