class CreateMaterialCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :material_categories do |t|
      t.string :name
      t.integer :sort

      t.timestamps
    end
  end
end
