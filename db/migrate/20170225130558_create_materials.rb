class CreateMaterials < ActiveRecord::Migration[5.1]
  def change
    create_table :materials do |t|
      t.string :name
      t.integer :node_id
      t.string  :color
      t.integer :alpha,default: 100
      t.string :thumbnail
      t.string :texture
      t.text   :remark
      t.text   :original_filename
      t.timestamps
    end
  end
end
