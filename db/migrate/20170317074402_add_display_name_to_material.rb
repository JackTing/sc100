class AddDisplayNameToMaterial < ActiveRecord::Migration[5.1]
  def change
    add_column :materials, :display_name, :string, default: ""
  end
end
