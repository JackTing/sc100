class CreateModelCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :model_categories do |t|
      t.string :name
      t.integer :sort
      t.integer :interior
      t.timestamps
    end
  end
end
