class AddDisplayNameToModel < ActiveRecord::Migration[5.1]
  def change
    add_column :models, :display_name, :string,default: ""
  end
end
