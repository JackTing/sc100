class CreateNodes < ActiveRecord::Migration[5.1]
  def change
    create_table :nodes do |t|
      t.string :name
      t.integer :sort
      t.integer :material_category_id
      t.integer :model_category_id
      t.timestamps
    end
  end
end
