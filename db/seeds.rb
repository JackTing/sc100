# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')

material_nodes = [
  {:name=>"玻璃",:sort=>0,:nodes=>[
      {:name=>"透明",:sort=>0},
      {:name=>"半透明",:sort=>1}
    ]
  },
  {:name=>"石材",:sort=>1,:nodes=>[
      {:name=>"大理石",:sort=>2},
      {:name=>"文化石",:sort=>3},
      {:name=>"毛石",:sort=>4},
      {:name=>"其它",:sort=>5}
    ]
  },
  {:name=>"金属",:sort=>2,:nodes=>[
      {:name=>"铝型材",:sort=>6},
      {:name=>"不锈钢",:sort=>7},
      {:name=>"其它",:sort=>8}
    ]
  },
   {:name=>"木材",:sort=>3,:nodes=>[
      {:name=>"原木",:sort=>9},
      {:name=>"有色木头",:sort=>10},
      {:name=>"其它",:sort=>11}
    ]
  },
   {:name=>"PVC",:sort=>4,:nodes=>[
      {:name=>"颜色",:sort=>12}
    ]
  },
   {:name=>"皮革",:sort=>5,:nodes=>[
      {:name=>"鳄鱼皮",:sort=>13},
      {:name=>"虎皮",:sort=>14},
      {:name=>"牛皮",:sort=>15},
      {:name=>"其它",:sort=>16}
    ]
  },
    {:name=>"液体",:sort=>6,:nodes=>[
      {:name=>"有色",:sort=>17},
      {:name=>"无色",:sort=>18}
    ]
  },
   {:name=>"植物",:sort=>7,:nodes=>[
      {:name=>"树",:sort=>19},
      {:name=>"灌木",:sort=>20},
      {:name=>"花草",:sort=>21},
      {:name=>"盆栽",:sort=>22},
      {:name=>"竹子",:sort=>23},
      {:name=>"藤蔓",:sort=>24},
      {:name=>"其它",:sort=>25}
    ]
  },
  {:name=>"油漆",:sort=>8,:nodes=>[
      {:name=>"开放漆",:sort=>26},
      {:name=>"封闭漆",:sort=>27}
    ]
  },
   {:name=>"墙面",:sort=>9,:nodes=>[
      {:name=>"乳胶漆",:sort=>28},
      {:name=>"硅藻泥",:sort=>29},
      {:name=>"壁纸",:sort=>30},
      {:name=>"其他",:sort=>31}
    ]
  },
   {:name=>"地面",:sort=>10,:nodes=>[
      {:name=>"地砖",:sort=>31},
      {:name=>"人行道",:sort=>33},
      {:name=>"马路",:sort=>34},
      {:name=>"木地板",:sort=>35},
      {:name=>"地坪漆",:sort=>36},
       {:name=>"其他",:sort=>37}
    ]
  },
    {:name=>"其他",:sort=>11,:nodes=>[
      {:name=>"其他",:sort=>38}
    ]
  }

]
material_nodes.each do |m|
  c  = MaterialCategory.create!(name:m[:name],sort:m[:sort])
  if m[:nodes]
    m[:nodes].each do |n|
      Node.create!(name:n[:name],sort:n[:sort],material_category_id:c.id)
    end
  end
end

model_nodes = [
  {:name=>"动物",:sort=>0,:nodes=>[
      {:name=>"飞行",:sort=>0},
      {:name=>"爬行",:sort=>1},
      {:name=>"水生",:sort=>2},
      {:name=>"哺乳",:sort=>3},
      {:name=>"其他",:sort=>4}
    ]
  },
  {:name=>"人物",:sort=>1,:nodes=>[
      {:name=>"男人",:sort=>5},
      {:name=>"女人",:sort=>6},
      {:name=>"多人",:sort=>7}

    ]
  },
   {:name=>"植物",:sort=>2,:nodes=>[
      {:name=>"树",:sort=>8},
      {:name=>"灌木",:sort=>9},
      {:name=>"花草",:sort=>10},
      {:name=>"盆栽",:sort=>11},
      {:name=>"竹子",:sort=>12},
      {:name=>"藤蔓",:sort=>13},
      {:name=>"其它",:sort=>14}
    ]
  },
   {:name=>"中式建筑",:sort=>3,:nodes=>[
      {:name=>"阁楼",:sort=>15},
      {:name=>"小建筑",:sort=>16},
      {:name=>"庭院",:sort=>17},
      {:name=>"其它",:sort=>18}
    ]
  },
   {:name=>"现代建筑",:sort=>4,:nodes=>[
      {:name=>"高楼",:sort=>19},
      {:name=>"小建筑",:sort=>20},
      {:name=>"其它",:sort=>21}
    ]
  },
   {:name=>"欧式建筑",:sort=>5,:nodes=>[
      {:name=>"高楼",:sort=>22},
      {:name=>"小建筑",:sort=>23},
      {:name=>"其它",:sort=>24}
    ]
  },
  {:name=>"其他建筑",:sort=>6,:nodes=>[
      {:name=>"其他",:sort=>27}
    ]
  },
  {:name=>"交通工具",:sort=>7,:nodes=>[
      {:name=>"汽车",:sort=>28},
      {:name=>"自行车",:sort=>29},
      {:name=>"飞机",:sort=>30},
      {:name=>"轮船",:sort=>31},
      {:name=>"电瓶车",:sort=>32}
    ]
  },
  {:name=>"城市设施",:sort=>8,:nodes=>[
      {:name=>"路灯",:sort=>33},
      {:name=>"花坛",:sort=>34},
      {:name=>"井盖",:sort=>35},
      {:name=>"公交站",:sort=>36},
      {:name=>"垃圾桶",:sort=>37},
      {:name=>"公共座椅",:sort=>38},
      {:name=>"广告牌",:sort=>39},
      {:name=>"交通指示牌",:sort=>40},
      {:name=>"信号灯",:sort=>41},
      {:name=>"桥梁",:sort=>42},
      {:name=>"地铁站",:sort=>43}
    ]
  },
  {:name=>"景观小品",:sort=>9,:nodes=>[
      {:name=>"喷泉",:sort=>44},
      {:name=>"花雕",:sort=>45},
      {:name=>"其它",:sort=>46}
    ]
  },
  {:name=>"室外其他",:sort=>10,:nodes=>[
      {:name=>"其他",:sort=>47}
    ]
  },
  {:name=>"沙发",:sort=>11,:nodes=>[
      {:name=>"中式",:sort=>48},
      {:name=>"现代",:sort=>49},
      {:name=>"欧式",:sort=>50},
      {:name=>"其他",:sort=>51}
    ]
  },
   {:name=>"桌椅",:sort=>12,:nodes=>[
      {:name=>"中式",:sort=>52},
      {:name=>"现代",:sort=>53},
      {:name=>"欧式",:sort=>54},
      {:name=>"其他",:sort=>55}
    ]
  },
   {:name=>"床",:sort=>13,:nodes=>[
      {:name=>"中式",:sort=>56},
      {:name=>"现代",:sort=>57},
      {:name=>"欧式",:sort=>58},
      {:name=>"其他",:sort=>59}
    ]
  },
   {:name=>"柜子",:sort=>14,:nodes=>[
      {:name=>"中式",:sort=>60},
      {:name=>"现代",:sort=>61},
      {:name=>"欧式",:sort=>62},
      {:name=>"其他",:sort=>63}
    ]
  },
   {:name=>"窗帘",:sort=>15,:nodes=>[
      {:name=>"中式",:sort=>64},
      {:name=>"现代",:sort=>65},
      {:name=>"欧式",:sort=>66},
      {:name=>"其他",:sort=>67}
    ]
  },
     {:name=>"单椅",:sort=>16,:nodes=>[
      {:name=>"中式",:sort=>68},
      {:name=>"现代",:sort=>69},
      {:name=>"欧式",:sort=>70},
      {:name=>"其他",:sort=>71}
    ]
  },
     {:name=>"灯具",:sort=>17,:nodes=>[
      {:name=>"中式",:sort=>72},
      {:name=>"现代",:sort=>73},
      {:name=>"欧式",:sort=>74},
      {:name=>"其他",:sort=>75}
    ]
  },
     {:name=>"装饰品",:sort=>18,:nodes=>[
      {:name=>"中式",:sort=>76},
      {:name=>"现代",:sort=>77},
      {:name=>"欧式",:sort=>78},
      {:name=>"其他",:sort=>79}
    ]
  },
     {:name=>"五金构建",:sort=>19,:nodes=>[
      {:name=>"中式",:sort=>80},
      {:name=>"现代",:sort=>81},
      {:name=>"欧式",:sort=>82},
      {:name=>"其他",:sort=>83}
    ]
  },
    {:name=>"室内其他",:sort=>20,:nodes=>[
      {:name=>"其他",:sort=>84}
    ]
  },
]
model_nodes.each do |m|
  c  = ModelCategory.create!(name:m[:name],sort:m[:sort])
  if m[:nodes]
    m[:nodes].each do |n|
      Node.create!(name:n[:name],sort:n[:sort],model_category_id:c.id)
    end
  end
end









