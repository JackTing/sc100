FactoryGirl.define do
  factory :material do
    name "MyString"
    material_category_id 1
    color "MyString"
    source "MyString"
  end
end
