FactoryGirl.define do
  factory :model do
    name "MyString"
    model_category_id 1
    color "MyString"
    source "MyString"
  end
end
