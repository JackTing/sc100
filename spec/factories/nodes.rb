FactoryGirl.define do
  factory :node do
    name "MyString"
    sort 1
    category_id 1
    category_type "MyString"
    interior 1
  end
end
